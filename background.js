let tabId;

// Start the scraping process
chrome.browserAction.onClicked.addListener((tab) => {
    tabId = tab.id;
    chrome.tabs.sendMessage(tabId, { action: "startScraping" });
});

// Listen for messages from content script
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === "scrapeCompleted") {
        // Handle completion of scraping on current page
        // Decide whether to navigate to the next page or not
        // If navigating to next page:
        chrome.tabs.update(tabId, { url: "nextPageURL" });
    }
});