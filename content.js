chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === "startScraping") {
        scrapeData().then(data => {
            // Process your data here
            downloadJSON(data, 'scraped-data.json');
            // Notify background script that scraping is completed
            chrome.runtime.sendMessage({ action: "scrapeCompleted" });
        }).catch(error => {
            console.error('Error during scraping:', error);
        });
    }
});


async function scrapeData() {
    const delayTime = 1000; // Delay time in milliseconds

    const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

    const clickElement = async (element) => {
        element.click();
        await delay(delayTime); // Wait for the page to load after clicking
    };

    const extractText = (html) => {
        return html.replace(/<[^>]*>/g, '')
            .replace(/&nbsp;/g, ' ')
            .replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#39;/g, "'")
            .replace(/[\u200B-\u200D\uFEFF]/g, '')
            .trim();
    };

    const scrapePage = (isCharacter) => {
        const nameElement = document.querySelector('.entry .thisname');
        const bioElement = document.querySelector('.entry ' + (isCharacter ? '.thisbio' : '.thisnotes'));
        const name = nameElement ? extractText(nameElement.innerHTML) : 'No Name';
        const bio = bioElement ? extractText(bioElement.innerHTML) : (isCharacter ? 'No Bio' : 'No Notes');
        return { name, bio };
    };

    const scrapeItems = async (listSelector, itemSelector, isCharacter) => {
        const itemList = document.querySelector(listSelector);
        if (!itemList) {
            console.log('No items found on this page.');
            return [];
        }

        const itemElements = itemList.querySelectorAll(itemSelector);
        const itemData = [];

        for (const element of itemElements) {
            await clickElement(element);
            itemData.push(scrapePage(isCharacter));
            history.back();
            await delay(delayTime);
        }

        return itemData;
    };

    // Combined data array for both characters and handouts
    const allData = [];

    // Scrape character data
    allData.push(...await scrapeItems('ul.characters', 'li.listing a.showentry', true));
    // Scrape handout data
    allData.push(...await scrapeItems('ul.handouts', 'li.listing a.showentry', false));

    console.log('Scraped data:', allData);
    return allData;
}

function downloadJSON(data, filename) {
    const jsonStr = JSON.stringify(data, null, 2);
    const blob = new Blob([jsonStr], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = filename || 'data.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}

// Starting the scraping process
scrapeData().then(data => {
    downloadJSON(data, 'scraped-data.json');
}).catch(error => {
    console.error('Error during scraping:', error);
});
